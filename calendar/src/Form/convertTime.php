<?php
function convertTime() {
    $keys = [
        'startDay',
        'NumberDay',
        'current_month',
        'current_year',
        'CurrentTime',
        'startDay',
        'TimeStart',
        'TimeEnd',
        'HourDay',
    ];
    $data = \Drupal::state()->getMultiple($keys);
    $time = 60*60*24;
    $startDay = $data['startDay'] + $time*$data['NumberDay'];
    $TableOrder = createTableOrder( $startDay,$data['NumberDay'],intval($data['TimeStart']),intval($data['TimeEnd']));
    $data['TableOrder'] = $TableOrder;
    return ($data);
}

/**
 * {@inheritdoc}
 */
function createTableOrder ($startDay,$numberDay,$TimeStart,$TimeEnd) {

    $rows ='<table><tbody><tr>';
    $time=0;
    //$link = array();
    $link = [];

   foreach (range(1, $numberDay) as $i) {
       $rows = $rows .
           '<th>' .
           date('d', ($startDay + $time)) . ' ' .
           date('F', ($startDay + $time)) .
           '</th>';
       $time = $time + 60 * 60 * 24;
       $link[strval($i)] = date('Y:F:d',($startDay + $time - 60 * 60 * 24));
      }
       $rows = $rows.'</tr>';

       foreach (range($TimeStart, $TimeEnd) as $j) {
           $rows = $rows . '<tr>';
           foreach (range(1, $numberDay) as $i) {
               $id =  strval($link[strval($i)])  . $j;
               $status = status($id)[0];
               /*id day*/
               $idTemp = strval($link[strval($i)]);
               $idArr = explode(":", $idTemp);
               $idDay = strtotime($idArr[2].' '.$idArr[1].' '.$idArr[0]);
               $statusDay = status($idDay)[0];
               /*******/

               if(intval(date(($idDay +intval($j)*60*60))) < time()){
                   $rows = $rows . '<td>' . $j . ':00 ' . ' time over</td>';
               }

               elseif ($statusDay != 0){
                   $rows = $rows . '<td>' . $j . ':00 ' . '<span>sessions<span></td>';
               } else {

                      if ($status == 0) {
                          $rows = $rows . '<td>' . $j . ':00 ' . '<a href="/messagemodal?id=' . $id . '" class="use-ajax link" data-dialog-type="modal">no classes' . '</a></td>';
                      } else {
                          $rows = $rows . '<td>' . $j . ':00 ' . '<span>sessions<span></td>';
                      }
               }
           }
           $rows = $rows . '</tr>';
       }
       $TableOrder = $rows. '</tbody></table>';
       return($TableOrder);
}

function status($id) {
    $number = array();
    $query = \Drupal::database()->select('calendar_evnt', 'nfd');
    $query->fields('nfd', ['calendar_id']);
    $query->condition('nfd.calendar_id', $id);
    $result = $query->execute()->fetchAll();
    $number[0] = count($result);

    if (is_int($id)){
        $idString = date('Y',$id).':'.date('F',$id).':'.date('d',$id);
        $query = \Drupal::database()->select('calendar_evnt', 'nfd');
        $query->fields('nfd', ['calendar_id']);
        $query->condition('nfd.calendar_id',  "%" . $query->escapeLike($idString ) . "%", 'LIKE');
        $result = $query->execute()->fetchAll();
        $number[1] = count($result);
    }

    return $number;
}

/**
 * {@inheritdoc}
 */
function createTableMonth ($startDay,$endDay,$numberDay) {

    $rows ='<table><tbody>';
    $nameDay = ['','Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
    $nameWeek = ['','I','II','III','IV','V','VI'];
    $numWeek = weeks( date('F',$startDay), date('Y',$startDay));

    foreach ($nameDay as $i) {
        $rows = $rows.'<tr>';
        foreach (range(0,$numWeek) as $j) {
            if ($i == '') {
                $rows = $rows . '<th>' . $nameWeek[$j] . '</th>';
            }
            if ($j == 0 and $i != '') {
                $rows = $rows . '<td class=NameDay>' . $i . '</td>';
            }
            if ($i != '' and $j > 0) {
                $id =  myrows($numberDay,$startDay,$i,$j)[1];
                $status = status($id);

                if($id < time()){
                    $rows = $rows . '<td>'.date('d',myrows($numberDay, $startDay, $i, $j)[1]).' time over</td>';
                }
 
                elseif ($status[0] == 0 and $status[1] == 0) {

                    $rows = $rows . '<td>' . '<a href="/messagemodal?id=' . $id .
                        '" class="use-ajax link" data-dialog-type="modal">' . myrows($numberDay, $startDay, $i, $j)[0] . '</a></td>';

                } else {
                    $rows = $rows . '<td>'.date('d',myrows($numberDay, $startDay, $i, $j)[1]).
                        '<span> sessions<span></td>';
                }
            }
        }
        $rows = $rows.'</tr>';
    }
    $TableOrder = $rows. '</tbody></table>';
    return $TableOrder;
}
/**
 * {@inheritdoc}
 */

function myrows($numberDay,$startDay,$i,$j) {
    $rows = array();
    foreach (range(0, $numberDay - 1) as $n) {
        $newDay = $startDay + $n * 60 * 60 * 24;
        $numberWeekDay = ((int) ((date("j", $newDay) + date("w", strtotime(date("m", $startDay) . "/01/" . date("Y", $startDay))) - 2) / 7)) + 1;
        if(date('D',$startDay) == 'Sun' and date('d',$newDay ) != 1) {
            $numberWeekDay = $numberWeekDay + 1;
        }
        if (date('D', $newDay) == $i and $numberWeekDay == $j) {
            $rows[0] =  '<span class="number">'.date('d', $newDay).'</span>'.' no classes';
            $rows[1] = $newDay;
            break;
        } else {
            $rows[0] =  '';
        }
    }
    return $rows;
}


function weeks($month, $year){
  $firstday = date("w", mktime(0, 0, 0, $month, 1, $year));
  $lastday = date("t", mktime(0, 0, 0, $month, 1, $year));
  $count_weeks = 1 + ceil(($lastday-7+$firstday)/7);
  return $count_weeks;
}