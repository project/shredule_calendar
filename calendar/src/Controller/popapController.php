<?php
/**
@file
Contains \Drupal\calendar\Controller\popapController.
 */

namespace Drupal\calendar\Controller;

use Drupal\Core\Controller\ControllerBase;

class popapController extends ControllerBase {
  public function popap() {

      $form = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\popapForm');
       return array (
       '#type' =>'markup',
       '#attached' => array(
           'library' => array(
               'calendar/calendar',
               ),
           ),
       '#markup'=> '<p>'.'</p>',
       $form,
       );
  }


}



