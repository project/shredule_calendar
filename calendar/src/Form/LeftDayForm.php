<?php

/**
 * @file
 * Contains \Drupal\calendar\Form\LeftDayForm.
 */

namespace Drupal\calendar\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;


class LeftDayForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'left_day_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

            $form['submit'] = array(
            '#type' => 'submit',
            '#value' => '<',
            '#ajax' => [
                    'callback' => '::ajaxSubmitCallback',
                    'event' => 'click',
                    'progress' => [
                        'type' => '',
                    ],
                ],
            '#button_type' => 'primary',
        );
        return $form;

    }

    /**
     * {@inheritdoc}
     */

    public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
      include_once ('convertTime.php');
      $ajax_response = new AjaxResponse();
      $time = 60*60*24;
      $data = convertTime();
      $startDay = $data['startDay'] - $time*$data['NumberDay'];

       $ajax_response->addCommand(new HtmlCommand('#day',
        date('d', ($data['startDay'] - $time*$data['NumberDay'])).'-'.
        date('d', (($data['startDay'] - $time*$data['NumberDay']) + $time*$data['NumberDay'] - $time   ))
      ));
      \Drupal::state()->setMultiple(['startDay'=>$startDay]);
      $ajax_response->addCommand(new HtmlCommand('#month', date('F',$startDay)));
      $ajax_response->addCommand(new HtmlCommand('#year', date('Y',$startDay)));
      $query =  createTableOrder( ($data['startDay'] - $time*$data['NumberDay']),$data['NumberDay'],intval($data['TimeStart']),intval($data['TimeEnd']));
      $ajax_response->addCommand(new HtmlCommand('#TableOrder',$query));
      return $ajax_response;
    }




    public function submitForm(array &$form, FormStateInterface $form_state) {
               

    }
}