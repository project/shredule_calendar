<?php

/**
 * @file
 * Contains \Drupal\calendar\Form\tableForm.
 */

namespace Drupal\calendar\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;


class tableForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'table_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $keys = [
            'startDay',
            'NumberDay',
            'current_month',
            'current_year',
            'CurrentTime',
            'startDay',
            'TimeStart',
            'TimeEnd',
            'HourDay'
        ];
        $data = \Drupal::state()->getMultiple($keys);


        include_once ('convertTime.php');
        if($data['HourDay'] == 'Day') {
            $d = '1 ';
            $m = strval(date('F', $data['startDay']));
            $y = strval(date('Y', $data['startDay']));
            $startDay = strtotime($d .$m.' '.$y);
            $numberDay =  date('t', $data['startDay']);
            $endDay = strtotime($numberDay.' ' .$m.' '.$y);
            $query = createTableMonth($startDay,$endDay,(int)$numberDay);

        }  else {

                $query = createTableOrder($data['startDay'], $data['NumberDay'], intval($data['TimeStart']), intval($data['TimeEnd']));
            }
       

        $form['table_order'] = array(

            '#markup' =>'<div id="TableOrder" class="TableOrder">'.$query.'</div>'
        );


        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => '>',
            '#ajax' => [
                'callback' => '::ajaxSubmitCallback',
                'event' => 'click',
                'progress' => [
                    'type' => '',
                ],
            ],
            '#button_type' => 'primary',
        );
        return $form;

    }

    /**
     * {@inheritdoc}
     */
    public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
      
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
               

      
    }
}