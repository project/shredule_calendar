<?php

/**
 * @file
 * Contains \Drupal\calendar\Form\SettingsForm.
 */

namespace Drupal\calendar\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class SettingsForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'mysettings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['hour_day'] = [
            '#type' => 'select',
            '#title' => $this->t('Select method'),
            '#options' => [
                'Hour' => 'Hour',
                'Day' => 'Day',
            ]
        ];
        $form['number_day'] = [
            '#type' => 'select',
            '#title' => $this->t('Select number days'),
            '#options' =>$this-> array_build(7),
            '#states' => [
                'visible' => [
                    ':input[name="hour_day"]' => array('value' => 'Hour'),
                ],
            ],



        ];
        $form['time_start'] = [
                    '#type' => 'select',
                    '#title' => $this->t('Select start time'),
                    '#options' =>$this-> array_build(24,'time'),
                    '#states' => [
                    'visible' => [
                    ':input[name="hour_day"]' => array('value' => 'Hour'),
                ],
            ],
        ];
        $form['time_end'] = [
                    '#type' => 'select',
                    '#title' => $this->t('Select end time'),
                    '#options' =>$this-> array_build(24,'time'),
                    '#states' => [
                    'visible' => [
                    ':input[name="hour_day"]' => array('value' => 'Hour'),
                ],
            ],
        ];
            $form['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
            '#button_type' => 'primary',
        );
        return $form;

    }
    public function array_build($number,$type){
       $result = array();
       foreach (range(1, $number) as $i) {
           if ($type =='time') {
               $result[$i] = $i.':00';
           } else {
               $result[$i] = $i;
           }
       }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
               
        $data = array(
        'HourDay' => $form_state->getValue(['hour_day']),
        'NumberDay' => intval($form_state->getValue(['number_day'])),
        'TimeStart' => $form_state->getValue(['time_start']),
        'TimeEnd' => $form_state->getValue(['time_end']),
        'CurrentTime' => time(),
        'startDay' =>  time(),
         );
         \Drupal::state()->setMultiple($data);
        $response = new \Symfony\Component\HttpFoundation\RedirectResponse('/order');
        $response->send();
        return $response;
      
    }
}