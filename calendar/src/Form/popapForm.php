<?php

/**
 * @file
 * Contains \Drupal\calendar\Form\popapForm.
 */

namespace Drupal\calendar\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;


class popapForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'popap_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {


        $form['email'] = array(
            '#type' => 'textfield',
            '#title' => t('Enter your email:'),
            '#required' => TRUE,
            '#ajax' => [
                // Если валидация находится в другом классе, то необходимо указывать
                // в формате Drupal\modulename\ClassName::methodName.
                'callback' => '::validateEmailAjax',
                // Событие, на которое будет срабатывать наш AJAX.
                'event' => 'change',
                // Настройки прогресса. Будет показана гифка с анимацией загрузки.
                'progress' => array(
                    'type' => 'throbber',
                    'message' => t('Verifying email..'),
                ),
            ],
            // Элемент, в который мы будем писать результат в случае необходимости.
            '#suffix' => '<div class="email-validation-message"></div>',



        );
        $form['message'] = array(
            '#type' => 'textfield',
            '#title' => t('Write your message:'),
            '#required' => TRUE,
        );
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
            '#button_type' => 'primary',
        );
        return $form;

    }





    /**
     * {@inheritdoc}
     */
    public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
        $response = new AjaxResponse();
        $pattern  ='/.+@.+(\.)\w\w\w/';
        $subject = $form_state->getValue('email');

        if (preg_match($pattern, $subject) == 0) {
            $response->addCommand(new HtmlCommand('.email-validation-message', 'This provider can lost our mail. Be care!'));
        }
        else {
            // Убираем ошибку если она была и пользователь изменил почтовый адрес.
            $response->addCommand(new HtmlCommand('.email-validation-message', ''));
        }
        return $response;
    }



    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
               
        $data = array(
        'email' => $form_state->getValue(['email']),
        'message' => $form_state->getValue(['message']),
        'id' => strval($_GET['id']),
        );
        $query = \Drupal::database()->insert('calendar_evnt');
        $query->fields([
            'calendar_id',
            'email',
            'message',
         ]);
        $query->values([
            $data['id'],
            
            $data['email'],
            $data['message'],
        ]);
        $response = new \Symfony\Component\HttpFoundation\RedirectResponse('/order');
        $response->send();
        return $response;
      
    }
}