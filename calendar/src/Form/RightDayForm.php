<?php

/**
 * @file
 * Contains \Drupal\calendar\Form\RightDayForm.
 */

namespace Drupal\calendar\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

class RightDayForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'right_day_form';
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $time = 60*60*24;
        $keys = [
            'HourDay',
            'NumberDay',
            'TimeStart',
            'TimeEnd' ,
            'CurrentTime',
            'startDay'
        ];
        $data = \Drupal::state()->getMultiple($keys);
        $start = date('d',$data['CurrentTime']);
        $end = $data['CurrentTime'] + ($data['NumberDay']-1)*60*60*24;
        $form['time_day'] = array(
            '#markup' =>  '<p id="day" class="day">'.$start.'-'.date('d',$end).'</p>'
        );
    $form['submit'] = array(
            '#type' => 'submit',
            '#value' => '>',
            '#ajax' => [
                'callback' => '::ajaxSubmitCallback',
                'event' => 'click',
                'progress' => [
                    'type' => '',
                ],
            ],
            '#button_type' => 'primary',
        );
        return $form;
    }
    public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
       include_once ('convertTime.php');
        $ajax_response = new AjaxResponse();
        $time = 60*60*24;
        $data = convertTime();
        $startDay = $data['startDay'] + $time*$data['NumberDay'];
        $endDay = $startDay + $time*$data['NumberDay'] - $time;
        $ajax_response->addCommand(new HtmlCommand('#day',
        date('d', ($data['startDay'] + $time*$data['NumberDay'])).'-'.
        date('d', $endDay)
        ));
        \Drupal::state()->setMultiple(['startDay'=>$startDay]);
        $ajax_response->addCommand(new HtmlCommand('#month', date('F',$startDay)));
        $ajax_response->addCommand(new HtmlCommand('#year', date('Y',$startDay)));
        $ajax_response->addCommand(new HtmlCommand('#TableOrder',$data['TableOrder']));
      return $ajax_response;
    }


  /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
               

    }


}