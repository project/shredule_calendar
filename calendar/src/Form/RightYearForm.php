<?php

/**
 * @file
 * Contains \Drupal\calendar\Form\RightYearForm.
 */

namespace Drupal\calendar\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;


class RightYearForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'right_year_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {



        $year = array();
        $year['current_year'] = date('Y');
        \Drupal::state()->setMultiple($year);

        $form['time_year'] = array(
            
            '#markup' =>  '<p id="year" class="year">'.date('Y').'</p>'
        );


        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => '>',
            '#ajax' => [
                'callback' => '::ajaxSubmitCallback',
                'event' => 'click',
                'progress' => [
                    'type' => '',
                ],
            ],
            '#button_type' => 'primary',
        );
        return $form;

    }

    /**
     * {@inheritdoc}
     */
    public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
      include_once ('convertTime.php');
      $ajax_response = new AjaxResponse();
      $data = convertTime();
      $dayInYear = date('L',$data['startDay']);
      $newYear = $data['startDay'] + (365 + $dayInYear)*60*60*24;
      $ajax_response->addCommand(new HtmlCommand('#month', date('F', $newYear)));
      $ajax_response->addCommand(new HtmlCommand('#day',
        date('d', $newYear).'-'.
        date('d', ($newYear + 60*60*24*$data['NumberDay'] - 60*60*24))
      ));
      $ajax_response->addCommand(new HtmlCommand('#year', date('Y',$newYear)));
      \Drupal::state()->setMultiple(['startDay'=>$newYear]);

        if($data['HourDay'] == 'Day') {
            $d = '1 ';
            $m = strval(date('F', $newYear));
            $y = strval(date('Y', $newYear));
            $startDay = strtotime($d .$m.' '.$y);
            $numberDay =  date('t', $newYear);
            $endDay = strtotime($numberDay.' ' .$m.' '.$y);
            $query = createTableMonth($startDay,$endDay,(int)$numberDay);
        } else {

            $query = createTableOrder($newYear, $data['NumberDay'], intval($data['TimeStart']), intval($data['TimeEnd']));
        }
        $ajax_response->addCommand(new HtmlCommand('#TableOrder',$query));

        return $ajax_response;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
               

      
    }
}