<?php

/**
 * @file
 * Contains \Drupal\calendar\Form\RightMonthForm.
 */

namespace Drupal\calendar\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;


class RightMonthForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'right_month_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
      include_once ('convertTime.php');
      $data = convertTime();
        $form['time_month'] = array(
            '#markup' =>  '<p id="month" class="month">'.date('F',$data['startDay']).'</p>'
        );
            $form['submit'] = array(
            '#type' => 'submit',
            '#value' => '>',
            '#ajax' => [
                    'callback' => '::ajaxSubmitCallback',
                    'event' => 'click',
                    'progress' => [
                        'type' => '',
                    ],
                ],
            '#button_type' => 'primary',
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
        include_once ('convertTime.php');
        $ajax_response = new AjaxResponse();
        $data = convertTime();
        $dayInMonth = date('t',$data['startDay']);
        $newMonth = $data['startDay'] + $dayInMonth*60*60*24;
        $ajax_response->addCommand(new HtmlCommand('#month', date('F', $newMonth)));
        $ajax_response->addCommand(new HtmlCommand('#day',
        date('d', $newMonth).'-'.
        date('d', ($newMonth + 60*60*24*$data['NumberDay'] - 60*60*24))
        ));
        $ajax_response->addCommand(new HtmlCommand('#year', date('Y',$newMonth)));
        \Drupal::state()->setMultiple(['startDay'=>$newMonth]);


       if($data['HourDay'] == 'Day') {
           $d = '1 ';
           $m = strval(date('F', $newMonth));
           $y = strval(date('Y', $newMonth));
           $startDay = strtotime($d .$m.' '.$y);
           $numberDay =  date('t', $newMonth);
           $endDay = strtotime($numberDay.' ' .$m.' '.$y);
           $query = createTableMonth($startDay,$endDay,(int)$numberDay);
       } else {
           $query =  createTableOrder( $newMonth, $data['NumberDay'],intval($data['TimeStart']),intval($data['TimeEnd']));
       }


        $ajax_response->addCommand(new HtmlCommand('#TableOrder',$query));
        return $ajax_response;
    }
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }
}