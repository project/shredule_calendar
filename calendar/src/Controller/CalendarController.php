<?php
/**
@file
Contains \Drupal\calendar\Controller\CalendarController.
 */

namespace Drupal\calendar\Controller;

use Drupal\Core\Controller\ControllerBase;

class CalendarController extends ControllerBase {
  public function content() {

      $form = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\SettingsForm');
       return array (
       '#type' =>'markup',
       '#attached' => array(
           'library' => array(
               'calendar/calendar',
               ),
           ),
       '#markup'=> '',
       $form,
       );
  }

    public function order(){

        $LeftDayForm = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\LeftDayForm');
        $RightDayForm = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\RightDayForm');
        $LeftMonthForm = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\LeftMonthForm');
        $RightMonthForm = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\RightMonthForm');
        $LeftYearForm = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\LeftYearForm');
        $RightYearForm = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\RightYearForm');
        $tableForm = \Drupal::formBuilder()->getForm('Drupal\calendar\Form\tableForm');

        $keys = [
            'startDay',
            'NumberDay',
            'current_month',
            'current_year',
            'CurrentTime',
            'startDay',
            'TimeStart',
            'TimeEnd',
            'HourDay',
        ];
        $data = \Drupal::state()->getMultiple($keys);

        if($data['HourDay'] == 'Hour') {

            return array(
                '#type' => 'markup',
                '#attached' => array(
                    'library' => array(
                        'calendar/calendar',
                        'core/drupal.dialog.ajax',
                    ),
                ),
                $LeftDayForm,
                $RightDayForm,
                $LeftMonthForm,
                $RightMonthForm,
                $LeftYearForm,
                $RightYearForm,
                $tableForm
            );

        } elseif ($data['HourDay'] == 'Day') {

            return array(
                '#type' => 'markup',
                '#attached' => array(
                    'library' => array(
                        'calendar/calendar',
                        'core/drupal.dialog.ajax',
                    ),
                ),
              
                $LeftMonthForm,
                $RightMonthForm,
                $LeftYearForm,
                $RightYearForm,
                $tableForm
            );

        }



    }


}



